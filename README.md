# [Proj1] Animador de algoritmos

Aluno: Diogo Cardoso Fernandes
Professor: Ricardo Inacio Alvares e Silva

# Roteiro

- [x] 7. No método paintComponent da classe Tocador, há duas funcionalidades desejadas, mas
     ainda não implementadas e descritas em comentários, para exibir certas informações na tela.
     Implemente-as.
- [x] 8. Na classe ListaGravada, implemente o método pintar. Ele é muito similar ao que você fez
     na seção 2.1, por isso utilize o seu código como base. ListaGravada possui como atributos
     uma lista de valores e outro de cores.

- [x] 9.  Na classe AlgoritmosAnimados, escreva um comentário em código para cada linha do
      método pesquisaSequencial, dando atenção especial às que envolvem uso do objeto
      Gravador anim. O objetivo é que você compreenda como utilizar o gravador para registrar
      o funcionamento dos algoritmos que você vai implementar a seguir.
- [x] 10. Na classe AlgoritmosAnimados, implemente a classificação pelo algoritmo da bolha no
      método homônimo. Após implementá-lo, utilize o Gravador para marcar os pontos de
      gravação do método, que são:
      • Disposição inicial da lista;
      • Cada vez que dois elementos são comparados, registre a operação, com o método
      adequado do Gravador;
      • Cada vez que ocorrer uma troca de posições entre elementos, registre-a;
      • Disposição final da lista;
- [x] 11. Na classe Gravador, faça a documentação no formato javadocs para todos os seus métodos.
      O objetivo é que você entenda o seu funcionamento, para ser capaz de escrever novos métodos
      de gravação quando for necessário.
- [x] 12. Na classe AlgoritmosAnimados, crie e implemente o método classificarPorSelecao,
      com as devidas marcações com o objeto de Gravação.
      Após terminar a implementação, é necessário ligá-la na interface gráfica. Para tal, crie
      uma entrada para ela dentro do switch do método onBtnCarregaPressionado na classe
      Animador. Para todas as outras implementações subsequentes, será necessário fazer o
      mesmo.

- [x] 13. Implemente, em AlgoritmosAnimados, o método pesquisaBinária. Os pontos de grava-
      ção a serem marcados são:

  • Disposição inicial;
  • Cada vez que o elemento for pesquisado em uma posição. Neste caso, deverá marcar
  também, com cores distintas, a posição inicial e final de pesquisa binária da lista.
  • Disposição final, com elemento encontrado destacado, caso houver;

* [ ] 14. Implemente o restante dos algoritmos animados: - Classificação por Inserção: deverá marcar as comparações, os deslocamentos à direita e
      as inserções. - Classificação Mergesort: deverá marcar as subdivisões, comparações e intercalação. - Classificação Quicksort: deverá marcar as subdivisões, comparações e trocas de posição.
      Marcar distintamente (utilizando outra cor) quando a troca for com o pivô.
