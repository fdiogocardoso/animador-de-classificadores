package br.unifil.dc.lab2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Write a description of class AlgoritmosAnimados here.
 * 
 * @author Ricardo Inacio
 * @version 20200408
 */
public class AlgoritmosAnimados
{
    public static Gravador listaEstatica(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Valores da lista imutável",false);
        return anim;
    }

    public static Gravador pesquisaSequencial(List<Integer> valores, int chave) {

        //INICIA GRAVADOR
        Gravador anim = new Gravador();
        //GRAVA A LISTA A SER UTILIZADA
        anim.gravarLista(valores, "Inicio de pesquisa sequencial",false);
        
        int i = 0;
        while (i < valores.size() && valores.get(i) != chave) {
            //DESTACA O INDICE 
            anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial");
             i++;
        }
        
        if (i < valores.size()) {
            //DESTACA O INDICE
            anim.gravarIndiceEncontrado(valores, i, "Chave encontrada");
        } else {
            //INFORMA QUE O INDICE NAO FOI ENCONTRADO
            anim.gravarLista(valores, "Chave não encontrada",false);
        }
        
        return anim;
    }

    public static Gravador pesquisaBinaria(List<Integer> lista, int chave) {
        Gravador anim = new Gravador();
        anim.gravarLista(lista, "Inicio Pesquisa Binária",false);
        boolean chaveEncontrada = false;
        int index = Integer.MAX_VALUE;
        int menor = 0;
        int maior = lista.size()-1;
        Collections.sort(lista);

        while (menor <= maior) {
            int meio = (menor + maior) / 2;
            anim.gravarComparacaoBinario(lista, menor, meio, maior);
            if (lista.get(meio) < chave) {
                menor = meio + 1;
                //anim.gravarComparacaoBinario(lista, menor, meio, maior);
            } else if (lista.get(meio) > chave) {
                //Sem o gravador não dá erro, porem precisa para o codigo antes de enviar comando ao gravador;
                if(meio >0){
                    chaveEncontrada = false;
                    break;
                }
                maior = meio - 1;
                anim.gravarComparacaoBinario(lista, menor, meio, maior);
            } else if (lista.get(meio) == chave) {
                index = meio;
                anim.gravarIndiceEncontrado(lista, index, "Chave encontrada");
                chaveEncontrada = true;
                break;
            }
        }
        if(!chaveEncontrada){
             anim.gravarLista(lista, "Chave não encontrada",false);
        }
        
		return anim;
	}
    
    
    public static Gravador classificarPorBolha(List<Integer> lista) {
        Gravador anim = new Gravador();
        anim.gravarLista(lista, "Inicio do BubbleSorte",false);
        
        boolean houvePermuta;
        do {
            houvePermuta = false;

           int i = 0;
           anim.gravarIndiceDestacado(lista, i, "BubbleSort");

            for (i = 1; i < lista.size(); i++) {
                anim.gravarIndiceDestacado(lista, i, "BubbleSort");
                if (lista.get(i-1) > lista.get(i)) {
                    anim.gravarComparacaoSimples(lista,i-1,i);
                    permutar(lista, i - 1, i);
                    anim.gravarPosTrocas(lista,i,i-1);
                    houvePermuta = true;
                }
            }
        } while (houvePermuta);
        
        anim.gravarLista(lista, "Fim do BubbleSorte",true);
        return anim;
    }



    public static Gravador classificarPorSelecao(List<Integer> lista) {
        Gravador anim = new Gravador();
        anim.gravarLista(lista, "Inicio SelectionSort",false);
        
        for (int i = 0; i < lista.size(); i++) {
            anim.gravarIndiceDestacado(lista, i, "SelectionSort");
            int menorIdx = encontrarIndiceMenorElem(anim,lista, i);
            anim.gravarMenorIndice(lista,menorIdx);
           
            if(menorIdx != i){
                anim.gravarComparacaoSimples(lista,i,menorIdx);
                permutar(lista, menorIdx, i);
                anim.gravarPosTrocas(lista,menorIdx,i);
            }
        
        }

        anim.gravarLista(lista, "Fim do SelectionSort",true);
        return anim;
    }


    /**
     * Classifica a lista em ordem crescente, pelo método de
     * inserção, in-place.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     */
    public static Gravador classificarPorInsercao(List<Integer> lista) {
        Gravador anim = new Gravador();
        anim.gravarLista(lista, "Inicio InsertionSort",false);

        for (int i = 1; i < lista.size(); i++) {
            Integer elem = lista.get(i);
            anim.gravarIndiceDestacado(lista, i, "Comparando elemento: " + elem);
            int j = i; 
            while (j > 0 && lista.get(j-1) > elem) {

                anim.gravarComparacaoSimples(lista,j,j-1);
                lista.set(j, lista.get(j-1)); // Deslocamento
               
                j--; 
                lista.set(j, elem);
                anim.gravarPosTrocas(lista,j,j+1);          
            }
        }
        anim.gravarLista(lista, "Fim do InsertionSort",true);
        return anim;
    }

    public static Gravador classificarMergeSort(List<Integer> lista) {
        Gravador anim = new Gravador();
        //SALVA A LISTA ORIGINAL
        anim.setLista(lista);
        anim.gravarLista(lista, "Inicio mergeSort",false);
        int contador =  0;
        mergeSort(lista,contador,anim);
       
            
        anim.gravarLista(lista, "Fim do mergeSort",true);
        return anim;
    }


    public static void mergeSort(List<Integer> lista,int contador, Gravador anim) {
        int tamanho = lista.size();
        
        if (tamanho < 2) {
            return;
        }
        int meio = tamanho / 2;

        //FAZ UM DEEP-COPY DA LISTA
        List<Integer> esquerdaL = new ArrayList<Integer>(); 
        lista.subList(0, meio).forEach((item) ->
          esquerdaL.add(item)
         ); 
       
        List<Integer> direitaL = new ArrayList<Integer>(); 
        lista.subList(meio, tamanho).forEach((item) ->
          direitaL.add(item)
         ); 

        anim.gravarSubdivisoesLista(lista,meio,contador);
        ++contador;
        mergeSort(esquerdaL,contador,anim );
        ++contador;
        mergeSort(direitaL,contador,anim);
        merge(lista, esquerdaL, direitaL, meio, tamanho - meio,anim, contador-1);
       

         
    }

    public static void merge(
        List<Integer> lista, List<Integer> esquerdaL, List<Integer> direitaL, int esquerda, int direita, Gravador anim, int contador) {
    
        int i = 0, j = 0, k = 0;
        while (i < esquerda && j < direita) {
            if (esquerdaL.get(i) <= direitaL.get(j)) {
                anim.setItemLista(lista,k, esquerdaL.get(i));
                lista.set(k++, esquerdaL.get(i++));
            }
            else {
                anim.setItemLista(lista,k, direitaL.get(j));
                lista.set(k++, direitaL.get(j++));
            }
        }
        while (i < esquerda) {
            //anim.setItemLista(lista,k, esquerdaL.get(i));
            lista.set(k++, esquerdaL.get(i++));
        }
        while (j < direita) {
            //anim.setItemLista(lista,k, direitaL.get(j));
            lista.set(k++, direitaL.get(j++));
           
        }
    }

    
    public static Gravador classificarQuickSort(List<Integer> lista) {
        Gravador anim = new Gravador();
        //SALVA A LISTA ORIGINAL
        //anim.setLista(lista);
        anim.gravarLista(lista, "Inicio QuickSort",false);
        int high = lista.size()-1;
        quicksort(lista,0,high,anim);
       
            
        anim.gravarLista(lista, "Fim do QuickSort",true);
        return anim;
    }


    private static void quicksort(  List<Integer> lista,int low, int high, Gravador anim) {
        
        int i = low, j = high;
        int pivo = lista.get(low + (high-low)/2);
        anim.gravarIndiceDestacado(lista,low + (high-low)/2,"Pivô");
       
        while (i <= j) {
           
            while (lista.get(i) < pivo) {
                i++;
            }
            while (lista.get(j) > pivo) {
                j--;
            }
            if (i <= j) {                     
                anim.gravarComparacaoSimples(lista,i,j);
                permutar(lista,i, j);
                anim.gravarPosTrocas(lista,j,i);
                i++;
                j--;
            }
        }
        if (low < j)
            quicksort(lista,low, j,anim);
        if (i < high)
            quicksort(lista,i, high,anim);
    }




    

    /**
     * Permuta (swap) dois elementos da lista de posição.
     * @param lista Lista cujos elementos serão permutados.
     * @param a Elemento a ser permutado.
     * @param b Elemento a ser permutado.
     */
    private static void permutar(List<Integer> lista, int a, int b) {
        Integer permutador = lista.get(a); 
        lista.set(a, lista.get(b)); 
        lista.set(b, permutador); 

    }

    /** 
     * Encontra o índice do menor elemento da lista.
     * @param anim, Gravadr para gravar a sequencia
     * @param lista - Lista que será pesquisada
     * @param idxInicio - Indicice inicial da pesquisa
     * @return Menor elemento encontrado
     */
    private static int encontrarIndiceMenorElem(Gravador anim,List<Integer> lista, int idxInicio) {
        int menor = idxInicio;
        for (int i = idxInicio+1; i < lista.size(); i++) {
             anim.gravarIndiceDestacado(lista, i, "Pesquisando menor elemento");
          
            if (lista.get(menor) > lista.get(i))
                menor = i;
        }
        return menor;
    }





}