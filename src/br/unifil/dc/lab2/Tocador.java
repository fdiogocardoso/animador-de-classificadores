package br.unifil.dc.lab2;

import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ListIterator;

/**
 * Write a description of class Tocador here.
 * 
 * @author Ricardo Inacio
 * @version 20200409
 */
public class Tocador extends JPanel {

    
  /**
	 *IDE GENERATED
	 */
    private static final long serialVersionUID = 1L;
    

    
	public Tocador(ListIterator<Transparencia> quadrosFilme) {
        setBackground(Color.WHITE);
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        carregarFilme(quadrosFilme);
    }
    public Tocador() {
        this(null);
    }
    
    public void carregarFilme(ListIterator<Transparencia> quadrosFilme) {
        this.quadrosFilme = quadrosFilme;
        this.quadroAtual = null;
        numQuadro = 0;
    }
    
    public Runnable avancarFilme() {
        if (quadrosFilme.hasNext()) {
            quadroAtual = quadrosFilme.next();
            numQuadro++;
        }
		return null;
    }
    
    public void voltarFilme() {
        if (quadrosFilme.hasPrevious()) {
            quadroAtual = quadrosFilme.previous();
            numQuadro--;
        }
    }
    
    public void rebobinarFilme() {
        while (quadrosFilme.hasPrevious()) {
            quadroAtual = quadrosFilme.previous();
            numQuadro--;
        }
    }

    public boolean autoFilme() {
        if (quadrosFilme.hasNext()) {
            quadroAtual = quadrosFilme.next();
            numQuadro++;
            
            return true;
        }
        return false;
    }


    
    public void playFilme() {
           while (quadrosFilme.hasNext()) {
                 quadroAtual = quadrosFilme.next();
            numQuadro++;
        }
    }



    
    
    

    
    protected void paintComponent(Graphics g) {
        Dimension dim = this.getSize();
        super.paintComponent(g);
        Graphics2D pincel = (Graphics2D) g;
        
        if (quadroAtual != null) {
            quadroAtual.pintar(pincel, this);
        } else {
           
            pincel.setColor(Color.RED);
            pincel.setFont(new Font("Monospaced", Font.PLAIN, 26));
            pincel.drawString("O Filme ainda não iniciou.",dim.width/4,dim.height/2);

            //Para sempre ter o quadro INFO
            pincel.setColor(Color.BLACK);
            pincel.setFont(new Font("Monospaced", Font.PLAIN, 16));
            pincel.drawString("INFO: " + "O Filme ainda não iniciou." ,20,dim.height-30);

        }
            pincel.setColor(Color.BLACK);
            pincel.setFont(new Font("Monospaced", Font.PLAIN, 16));
            pincel.drawString("Quadro: " + numQuadro ,20,dim.height-10);     
        
    }

   
    private int numQuadro = 0;
    private Transparencia quadroAtual = null;
    private ListIterator<Transparencia> quadrosFilme = null;
}
