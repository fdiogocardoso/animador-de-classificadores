package br.unifil.dc.lab2;

import javax.swing.JPanel;
import java.awt.*;
import java.util.Collections;
import java.util.List;


/**
 * Write a description of class ListaGravada here.
 * 
 * @author Ricardo Inacio
 * @version 20200409
 */
public class ListaGravada implements Transparencia
{
    /**
     * Constructor for objects of class ListaGravada
     */
    public ListaGravada(List<Integer> lista, List<Color> coresIndices, String nome) {
        this.lista = lista;
        this.nome = nome;
        this.coresIndices = coresIndices;
    }
    

    public static void desenharColuna(Graphics2D g,int x,int tamanhoX,int tamanhoY, Color cor) {
            g.setColor(cor);
            g.fillRect(x, 450-tamanhoY, tamanhoX,tamanhoY );
            g.setColor(Color.BLACK);
            g.setStroke(new BasicStroke(3));
            g.drawRect(x, 450-tamanhoY, tamanhoX,tamanhoY );
    }



    public void pintar(Graphics2D pincel, JPanel contexto) {
        Dimension dim = contexto.getSize();
        Integer tamanhoLista = this.lista.size();
        Integer max = Collections.max(this.lista);
        Integer xInicial = 20;
        Integer espacamento = 50;
        Integer tamanhoX = (dim.width - xInicial*2 - espacamento*(tamanhoLista-1))/tamanhoLista;

        if(tamanhoX < 50){
            espacamento = tamanhoX/2;
            if(espacamento < 5) espacamento = 5;
            //RECALCULA tamanhoX CASO ESPAÇAMENTO DIMINUA
            tamanhoX = (dim.width - xInicial*2 - espacamento*(tamanhoLista-1))/tamanhoLista;
        }

        for(int i=0;i<tamanhoLista;i++){
             //REGRA DE 3
            Integer tamanhoY = this.lista.get(i)*300/max;
            
            desenharColuna(pincel,xInicial+(tamanhoX+espacamento)*i,tamanhoX,tamanhoY,this.coresIndices.get(i));

            pincel.setColor(Color.RED);
            pincel.setFont(new Font("Monospaced", Font.PLAIN, 16));
            pincel.drawString(Integer.toString(this.lista.get(i)),(tamanhoX/3) + xInicial+(tamanhoX+espacamento)*i,475);
        }

       

            pincel.setColor(Color.BLACK);
            pincel.setFont(new Font("Monospaced", Font.PLAIN, 16));
            pincel.drawString("INFO: " + this.nome ,20,dim.height-30);
       
    }
    
    
    private List<Integer> lista;
    private List<Color> coresIndices;
    private String nome;
}
