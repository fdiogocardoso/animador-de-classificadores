package br.unifil.dc.lab2;

import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;
import java.awt.Color;

/**
 * Write a description of class Gravador here.
 *
 * @author Ricardo Inacio
 * @version 20200409
 */
public class Gravador
{
    public Gravador() {
        this.seqGravacoes = new ArrayList<Transparencia>();
        this.listOriginal = new ArrayList<Integer>(); 
    }

    
    /** Método para gravar a lista completa,e  seu estado
     * @param lista - A lista que deseja gravar
     * @param nome - String com o nome do método ou informação 
     * @param isFim - Indica se é o fim da ordenação para que as cores fiquem diferentes
     */
    public void gravarLista(List<Integer> lista, String nome, Boolean isFim) {
        List<Color> cores;
       if(isFim){
            cores = novaListaColors(lista.size(), Color.GREEN);
       }
       else{
           cores = novaListaColors(lista.size(), Color.BLUE);
       }
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, nome);
        seqGravacoes.add(gravacao);
    }



    public void gravarListaOrdenada(List<Integer> lista, String nome) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, nome);
        seqGravacoes.add(gravacao);
    }

    
    /** Método que grava o Indice que será destacado na tela, é o indice que está atualmente sendo "observado"
     * @param lista - A lista completa
     * @param i - O indice que está sendo observado
     * @param nome - o nome da "ação"
     */
    public void gravarIndiceDestacado(List<Integer> lista, int i, String nome) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(i, Color.MAGENTA);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, nome);
        seqGravacoes.add(gravacao);
    }

    
    /**  Método que grava a comparação que está sendo feita, e prepara para a exibição em tela, criando cores para destaca-los
     * @param lista - A lista completa
     * @param i - Um dos indices da comparação
     * @param j - Um dos indices da comparação
     */
    public void gravarComparacaoSimples(List<Integer> lista, int i, int j) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(i, Color.GRAY);
        cores.set(j, Color.GRAY);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, "Comparação");
        seqGravacoes.add(gravacao);
    }


    public void gravarComparacaoBinario(List<Integer> lista, int menor, int meio,int maior) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(menor, Color.ORANGE);
        cores.set(meio, Color.GREEN);
        cores.set(maior,Color.ORANGE);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, "Comparação Binária");
        seqGravacoes.add(gravacao);
    }

    
    /**  Método que grava a troca que foi feita, e prepara para a exibição em tela, criando cores para destaca-los
     * @param lista - A lista completa
     * @param i - Um dos indices da comparação
     * @param j - Um dos indices da comparação
     */
    public void gravarPosTrocas(List<Integer> lista, int i, int j) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(i, Color.YELLOW);
        cores.set(j, Color.YELLOW);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, "Pós-troca");
        seqGravacoes.add(gravacao);
    }


    public void gravarMenorIndice(List<Integer> lista, int i) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(i, Color.GREEN);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, "Menor valor");
        seqGravacoes.add(gravacao);
    }


    public void gravarIndiceEncontrado(List<Integer> lista, int i, String nome) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(i, Color.RED);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, nome);
        seqGravacoes.add(gravacao);
    }

    public void gravarSubdivisoesLista(List<Integer>lista, int meio, int contador ) {
        List<Color> cores = novaListaColors(this.listOriginal.size(), Color.BLUE);
   

        if(contador>=this.listOriginal.size()/2){
            for (int i = contador; i < this.listOriginal.size()-meio; i++) {
                cores.set(i, Color.MAGENTA);       
            }
            for (int i = this.listOriginal.size()-meio; i < this.listOriginal.size(); i++) {
                cores.set(i, Color.CYAN);       
            }  
       } else{
            for (int i = 0; i < meio; i++) {
                cores.set(i, Color.MAGENTA);       
            }
            for (int i = meio; i < lista.size(); i++) {
                cores.set(i, Color.CYAN);       
            }  
       
       }      
       
        ListaGravada gravacao = new ListaGravada(List.copyOf(this.listOriginal), cores, "Subdivisão");
        seqGravacoes.add(gravacao);
    }


    
    /** 
     * @return ListIterator<Transparencia>
     */
    public ListIterator<Transparencia> getFilme() {
        return seqGravacoes.listIterator();
    }
    public void setLista(List<Integer> listOriginal) {
            
        listOriginal.forEach((item) ->
          this.listOriginal.add(item)
         ); 
       
    }

    public void setItemLista(List<Integer> listOriginal, int index, int valor) {
        this.listOriginal.set(index,valor);
    }
    public List<Integer> getLista() {
        return listOriginal;
    }

    

    
    /** Cria uma lista de cores, para auxiliar o método pintar
     * @param numElems - O tamanho da lista
     * @param cor -  a cor padrão da lista
     * @return List<Color>
     */
    private static List<Color> novaListaColors(int numElems, Color cor) {
        ArrayList<Color> lista = new ArrayList<>(numElems);
        for (; numElems > 0; numElems--) lista.add(cor);
        return lista;
    }

    private List<Transparencia> seqGravacoes;
    //TENTANDO NAO TER DE TROCAR O METODO RECURSIVO
    public List<Integer> listOriginal;
}
